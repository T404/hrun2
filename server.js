
var express, app, server, fs, path, request, ws, nodemailer, mongojs;

var port = process.env.OPENSHIFT_NODEJS_PORT || 3002;
var ip = process.env.OPENSHIFT_NODEJS_IP  || 0;

express = require('express');
app = express();
mongojs = require('mongojs');

// Connecting mongoDB
var db = mongojs('corona:coronadbpass@ds037698.mongolab.com:37698/hungryrun');
var highscores = db.collection('highscores');

// Configuration
app.configure(function(){

	app.use(express.urlencoded());
	app.use(express.json());

	app.use(express.cookieParser()); // Cookies

	app.use(express.logger({ format: ' :req[ip] :date (:response-time ms): :method :url' })); // Logging
	app.use(express["static"]('./static')); // Static files
});

// Renders main
app.get('/', function(req, res) {

	res.render('index');
});

app.post('/getScores', function(req, res) {

	//console.log(req.body)
	highscores.find().sort({score:-1}, function(err, ans) {

		res.send(ans);
	});
});

app.post('/insertScore', function(req, res) {

	highscores.find().sort({score:1}, function(err, docs) {

    	allScore = docs

 		if (!err && docs) {
    
            console.log(docs[0].score, req.body.score)

            if (docs[0].score <= req.body.score) {

                var userdata = {name: req.body.name, score: req.body.score};
                highscores.insert(userdata);

                res.send("inserted");
                console.log('Inserted');

            } else {

                res.send("notInserted");
                console.log('Not Inserted');
            };

        } else {

            res.send("notInserted");
            console.log('Not Inserted');
        };
    });
});

// Set server
server = app.listen(port, ip);
console.log(' - HTTP server started on port: ' + port + ', ip: ' + ip + '\n');